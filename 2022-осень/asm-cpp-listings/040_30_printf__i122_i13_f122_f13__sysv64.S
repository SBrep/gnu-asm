// Макрос для компенсации искажения имён в Mac OS X (и в MS Windows 32, но в этой программе неактуально)
#ifdef __APPLE__
#define FNAME(s) _##s
#elif _WIN64
#define FNAME(s) s
#elif _WIN32
#define FNAME(s) _##s
#else
#define FNAME(s) s
#endif 

.data 
    fmt1: .string "x = %d, y = %d\n"
    x1:   .int +122
    
    fmt2: .string "x = %lf, y = %lf\n"
    x2:   .double +122
   
.text
.globl FNAME(main)

FNAME(main):        
    push %rbp 
    
    lea fmt1(%rip), %rdi
    mov x1(%rip), %esi
    mov $-13, %edx
    mov $0, %al   
    call FNAME(printf)
     
    lea fmt2(%rip), %rdi
    movsd x2(%rip), %xmm0
    mov $-13, %r8d
    cvtsi2sd %r8d, %xmm1 // преобразование long → double
    mov $2, %al   
    call FNAME(printf)
    
    pop %rbp 
    xor %eax, %eax 
    ret 
