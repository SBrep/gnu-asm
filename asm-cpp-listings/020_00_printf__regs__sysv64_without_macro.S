// www.onlinegdb.com
// https://www.jdoodle.com/compile-assembler-gcc-online/

.data 
    fmt: .string " A=%016llX,  C=%016llX,   D=%016llX,   B=%016llX, si=%016llX, di=%016llX,\nr8=%016llX, r9=%016llX, r10=%016llX, r11=%016llX\n xmm0[0] = %5lg, xmm1[0] = %5lg, xmm2[0] = %5lg, xmm3[0] = %5lg, xmm4[0] = %5lg, xmm5[0] = %5lg, xmm6[0] = %5lg, xmm7[0] = %5lg" 
    
    a: .quad 0xFFFFFFFF0000000A
    
    x: .double 3.11
   
.text
.globl main

main:        
    push %rbx 
    // 11 [fmt, A, C, D, B, di, si, r8, r9, r10, r11] - 6 [di, si, D, C, r8, r9] = 5
    sub $(8*6), %rsp 
    
    // A = xmm0 = 9
    mov $9, %rax    
    vcvtsi2sd %rax, %xmm0, %xmm0
    
    // C = xmm1 = 3.11
    movsd x(%rip), %xmm1
    vcvtsd2si %xmm1, %rcx
    
    // D = A - C = 9-3 = 6
    mov %rax, %rdx
    sub %rcx, %rdx
    
    // xmm2 = xmm0-xmm1 = 9.00 - 3.11 = 5.89    
    vsubsd %xmm1, %xmm0, %xmm2
     
    // xmm3 = static_cast<double>(a как 32 или 64 бита)  
    vcvtsi2sdl a(%rip), %xmm3, %xmm3 // lo(A) = 10 => xmm3
    vcvtsi2sd  a(%rip), %xmm4, %xmm4 // то же самое, то есть по умолчанию l
    vcvtsi2sdq a(%rip), %xmm5, %xmm5 // A = -2^32+10 ≈ −4.2⋅10^9 => xmm5
    
   
        
    // SysV amd64: di, si, D, C, r8, r9, xmm0-xmm7 + al
    mov %rdi,   (%rsp)
    mov %r8,   8(%rsp)
    mov %r9,  16(%rsp)
    mov %r10, 24(%rsp)
    mov %r11, 32(%rsp)
    mov %rsi, %r9
    
    lea fmt(%rip), %rdi
    mov %rax, %rsi
    xchg %rcx, %rdx
    mov %rbx, %r8

    mov $8, %al 
    call printf

    add $(8*6), %rsp 

    pop %rbx
    xor %eax, %eax 
    ret 
