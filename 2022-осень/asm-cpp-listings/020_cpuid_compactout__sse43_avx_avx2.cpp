#include <cstdlib>
#include <cstdio>
#include <cstring>

#define PERFORM_AND_CPUID_TEST(x) x; asm("cpuid\n": "+a"(A), "+b"(B), "+c"(C), "+d"(D));

#define PRINT_FLAG(tag, r, b) printf("%s=%X  ", #tag, ( r & (1<<b) ) !=0 );

int main() 
{
    unsigned int A = -1, B = -1, C = -1, D = -1;
    
    PERFORM_AND_CPUID_TEST(A = 1;)    
    PRINT_FLAG(SSE4.3, C, 20)
    PRINT_FLAG(AVX, C, 28)

    PERFORM_AND_CPUID_TEST(A = 0x80000001;)
    PRINT_FLAG(AMD XOP, C, 11)
    
    PERFORM_AND_CPUID_TEST(A = 7; C = 0;)
    PRINT_FLAG(AVX2, B, 5)
    
    puts("");
    
    return 0;
}
